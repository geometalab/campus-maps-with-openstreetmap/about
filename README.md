# Campus Maps with OpenStreetMap

This repository covers several approaches (starting with uMap) on how to implement a Campus Map using OpenStreetMap as thematic layers (e.g. for buildings and rooms) and for the base maps. It's also aka list of awesome Campus Map examples.

## Examples

Stufen: U uMap, T rein HTML5/JS (responsive), R Routing/Navigation, (N native)
Inspirations:

Campus-Tour mit 360 Grad-Fotografien und Koordinaten:
https://vtour.icom4u.ch/vtour/tour.html?startscene=45&startactions=lookat(-25.04,-2.61,140,0,0);
Ein paar wichtige Locations an der HSR bzw. OST Campus Rappi: https://giswiki.hsr.ch/Kategorie:HSR


Bekannte Campus Maps (Stufen I-III):

U - Campus Map of Université libre de Bruxelles (ULB): https://umap.openstreetmap.fr/de/map/ulb-campus-solbosch_228397

T - Oxford and Cambridge universities official maps: https://map.cam.ac.uk/ , https://maps.ox.ac.uk/ .  Details about how the Cambridge one is maintained at
https://map.cam.ac.uk/help/ .
R - TU Dresden has a nice campus navigator with indoor maps, accessibility
information and routing, among other features. Partly based on OSM and
open-source components such as GraphHopper for routing: https://navigator.tu-dresden.de/ (Stellenausschreibung mit Technologien (PDF))
R - EPFL (Eidg. Polytechnikum Lausanne) Plan: https://plan.epfl.ch/


## OSM Indoor Maps:

- [OsmInEdit](https://osminedit.pavie.info/#20/47.22330/8.81834/0)

- [OpenLevelUp!](https://openlevelup.net/?l=0#19/47.22326/8.81705)

- [Indoor=](https://indoorequal.org/#map=17.13/47.223427/8.8173)

## Other campus maps:

- [OSM talk thread](https://lists.openstreetmap.org/pipermail/talk/2021-February/086207.html)

- [Goog search result list](https://www.google.com/search?client=firefox-b-d&q=awesome+campus+maps)

- [Pinterest list](https://www.pinterest.com/leekreindel/campus-maps/)


